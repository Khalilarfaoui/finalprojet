<?php

namespace App\Tests;

use App\Entity\Client;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testClient()
    {
        // Arrange
        $client = new Client();

        // Act
        $client->setNom('Khalil');

        // Assert
        $this->assertEquals('Khalil', $client->getNom());
    }
}
