<?php

namespace App\Tests;

use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;

class VoitureTest extends TestCase
{
    public function testVoiture()
    {
        // Arrange
        $voiture = new Voiture();

        // Act
        $voiture->setSerie('147TUN2255');

        // Assert
        $this->assertEquals('147TUN2255', $voiture->getSerie());
    }
}
