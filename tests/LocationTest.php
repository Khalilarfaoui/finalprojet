<?php

namespace App\Tests;

use App\Entity\Location;
use PHPUnit\Framework\TestCase;

class LocationTest extends TestCase
{
    public function testLocation()
    {
        // Arrange
        $location = new Location();

        // Act
        $location->setPrix(30000);

        // Assert
        $this->assertEquals(30000, $location->getPrix());
    }
}
